include play
play::application { "ecompliance-semantic" :
	source => 'https://files.bmtresearch.org/data/public/be1824.php?dl=true',
	version => '1.0',
	port => '9000',
	config_file => '/vagrant/puppet/application.conf'
}

