package rdf

/**
 * Created by droythorne on 11/05/15.
 */

import com.hp.hpl.jena.rdf.model.{AnonId, Model}
import com.hp.hpl.jena.vocabulary.RDF
import orm.Tables.RegulationRow
class RDFRegulation {

  def GenerateMetalexFragment(r : RegulationRow, m : Model, prefix : String): Unit = {
   // val fragment = m.createResource(prefix + r.id + "_" + r.regulationtitle.filterNot { case whiteSpace => true })
    val fragment = m.createResource(AnonId.create().toString, MetalexTerms.Fragment)
     .addProperty(DCTerms.title, r.regulationtitle)
     .addProperty(RDF.value, m.createTypedLiteral(r.regulationtext))
  }
}