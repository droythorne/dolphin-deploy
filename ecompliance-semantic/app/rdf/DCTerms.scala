package rdf

import com.hp.hpl.jena.ontology.{AnnotationProperty}

/**
 * Created by droythorne on 11/05/15.
 */


object DCTerms extends RDFModelVocabulary {
  override val config_path = "rdf.owl.dcterms"
  override val ns_prefix = "dcterms"
  lazy val title : AnnotationProperty = m.getAnnotationProperty(NS + "title")
}



