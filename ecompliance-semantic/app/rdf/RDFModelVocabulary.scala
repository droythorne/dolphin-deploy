package rdf

import com.hp.hpl.jena.ontology.{OntModelSpec, AnnotationProperty, OntModel}
import com.hp.hpl.jena.rdf.model.{Model, ModelFactory}
import com.hp.hpl.jena.util.FileManager
import play.Play

/**
 * Created by droythorne on 11/05/15.
 */
class RDFModelVocabulary {

  //Make sure this is filled in!
  val config_path: String = ""
  val ns_prefix: String = ""
  lazy val SOURCE = getSource(config_path)
  lazy val m: OntModel = getModel
  lazy val NS : String = m.getNsPrefixURI(ns_prefix)

  protected def getSource(config_path: String): String = {
    return Play.application.configuration().getString(config_path)
  }

  protected def getModel: OntModel = {
    val m =  ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM)
    loadData(m)
    return m
  }

  protected def loadData(m: Model) {
    FileManager.get.readModel(m, SOURCE)
  }
}



