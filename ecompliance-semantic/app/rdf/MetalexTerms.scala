package rdf

import com.hp.hpl.jena.ontology.{ObjectProperty, AnnotationProperty}
import com.hp.hpl.jena.rdf.model.Resource

/**
 * Created by droythorne on 11/05/15.
 */
object MetalexTerms extends RDFModelVocabulary {
  override val config_path = "rdf.owl.metalex"
  override val ns_prefix = "metalex"
  lazy val BibliographicWork : Resource = m.getResource(NS + "BibliographicWork")
  lazy val BibliographicExpression : Resource = m.getResource(NS + "BibliographicExpression")
  lazy val Fragment : Resource = m.getResource(NS + "Fragment")
  lazy val Document : Resource = m.getResource(NS + "Document")
  lazy val BibliographicCreation: Resource = m.getResource(NS + "BibliographicCreation")
  lazy val Legislator = m.getResource(NS + "Legislator")
  lazy val resultOf = m.getProperty(NS + "resultOf")
  lazy val agentOf = m.getProperty(NS + "agentOf")
  lazy val embodies = m.getProperty(NS + "embodies")
  lazy val realizes = m.getProperty(NS + "realizes")
}
