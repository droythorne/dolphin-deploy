package dao

/**
 * Created by droythorne on 06/05/15.
 */

import orm.Tables.RegulationRow
import play.api.Play
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.profile.RelationalProfile

import scala.concurrent.Future
import orm.Tables._

class RegulationDAO extends HasDatabaseConfig[RelationalProfile] {
  protected val dbConfig = DatabaseConfigProvider.get[RelationalProfile](Play.current)
  import dbConfig.driver.api._

  def all(): Future[List[RegulationRow]] = dbConfig.db.run(Regulation.result).map(_.toList)
  def root_regulations(): Future[Seq[(Long, Seq[Long])]] = {
    val q = SubRegulation.map{r => (r.regulationid, r.subregulationid)}
    dbConfig.db.run(q.result.map{ _.groupBy(_._1).map {
      case (k,v) => (k, v.map(_._2))}.toSeq})
  }

}
