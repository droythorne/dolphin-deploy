package dao

/**
 * Created by droythorne on 06/05/15.
 */

import play.api.Play
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import slick.profile.RelationalProfile

import orm.Tables._

import scala.concurrent.Future

class DefinitionItemDAO extends HasDatabaseConfig[RelationalProfile] {
  protected val dbConfig = DatabaseConfigProvider.get[RelationalProfile](Play.current)
  import dbConfig.driver.api._

  def all(): Future[List[DefinitionitemRow]] = dbConfig.db.run(Definitionitem.result).map(_.toList)
}
