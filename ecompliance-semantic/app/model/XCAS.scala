package model

/**
 * Created by droythorne on 01/05/15.
 */
import scala.annotation.meta
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}


object Language extends Enumeration {
  type Language = Value
  val en = Value("en")
  val no = Value("no")
  val fr = Value("fr")
  val es = Value("es")
  val de = Value("de")
}
import Language._

object SKOSConceptProperty extends Enumeration {
  type SKOSConceptProperty = Value
  val altLabel, prefLabel, hiddenLabel = Value
}

import SKOSConceptProperty._


