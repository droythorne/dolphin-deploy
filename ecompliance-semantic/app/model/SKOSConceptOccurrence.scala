package model

import model.Language.Language
import model.SKOSConceptProperty.SKOSConceptProperty

/**
 * Created by droythorne on 01/05/15.
 */
case class SKOSConceptOccurrence( iri: String,
                                  text: String,
                                  begin: Int,
                                  end: Int,
                                  language: Language,
                                  SKOS_concept_property: SKOSConceptProperty) {
}
