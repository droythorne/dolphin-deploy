package controllers

import play.api.mvc._
import model._
object SKOS extends Controller {

  def freetext(): Action[String] = Action (parse.text) { request =>
    val text : String = request.body
    val concept_test = new SKOSConceptOccurrence("http://e-compliance.tetrahedra.eu/skos/concept/#fishingvessel",
      text,
      6,
      17,
      Language.en,
      SKOSConceptProperty.altLabel)
    Ok(views.xml.skosconceptoccurrence.render(concept_test))
  }

  def uri_request() = Action(parse.xml) { request =>
    (request.body \\ "URIRequest" \\ "iri" headOption).map(_.text).map { name =>
      Ok("Hello " + name)
    }.getOrElse {
      BadRequest("Failed to parse xml")
    }
  }
}