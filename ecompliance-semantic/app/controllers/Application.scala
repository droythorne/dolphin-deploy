package controllers

import dao.DefinitionItemDAO
import play.api._
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global

object Application extends Controller {

  def dao = new DefinitionItemDAO

  def index = Action.async {
    dao.all().map(c => Ok(views.html.index(c)))
  }
}