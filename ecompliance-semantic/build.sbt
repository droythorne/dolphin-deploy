import ScalaxbKeys._

lazy val commonSettings = Seq(
  name := "ecompliance-semantic",
  version := "1.0",
  organization  := "com.example",
  scalaVersion  := "2.11.5"
)

lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % "1.0.2"
lazy val scalaParser = "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1"
lazy val dispatchV = "0.11.2"
lazy val dispatch = "net.databinder.dispatch" %% "dispatch-core" % dispatchV

lazy val slick = taskKey[Seq[File]]("gen-tables")

lazy val slickCodeGenTask = (sourceManaged, dependencyClasspath in Compile, runner in Compile, streams) map { (dir, cp, r, s) =>
  val outputDir = (dir / "slick").getPath // place generated files in sbt's managed sources folder
val url = "jdbc:mysql://172.22.2.4/creationtool"
  val jdbcDriver = "com.mysql.jdbc.Driver"
  val slickDriver = "slick.driver.MySQLDriver"
  val pkg = "orm"
  println(cp)
  toError(r.run("slick.codegen.SourceCodeGenerator", cp.files, Array(slickDriver, jdbcDriver, url, outputDir, pkg, "devel", "3EneOVwRuwLv"), s.log))
  val fname = outputDir + "/orm/Tables.scala"
  Seq(file(fname))
}

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases"

resolvers += Resolver.typesafeIvyRepo("releases")

resolvers += Resolver.typesafeIvyRepo("snapshots")

resolvers += Resolver.typesafeRepo("releases")

resolvers += Resolver.typesafeRepo("snapshots")

resolvers += "Apache repo releases" at "http://repository.apache.org/content/repositories/releases/"

val slick_dep = "com.typesafe.slick" %% "slick" % "3.0.0"
val slickcodegen_dep = "com.typesafe.slick" %% "slick-codegen" % "3.0.0"
val playslick_dep = "com.typesafe.play" %% "play-slick" % "1.0.0-2015-05-07-960743d-SNAPSHOT"
val mysql_dep = "mysql" % "mysql-connector-java" % "5.1.34"

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(dispatch),
    libraryDependencies ++= {
      if (scalaVersion.value startsWith "2.11") Seq(scalaXml, scalaParser)
      else Seq()
    },
    libraryDependencies ++= Seq(slick_dep, slickcodegen_dep, playslick_dep, mysql_dep),
    libraryDependencies ++= Seq(
      jdbc,
      cache,
      ws
    ),
    libraryDependencies += specs2 % Test,
    libraryDependencies += "org.apache.jena" % "apache-jena-libs" % "2.13.0"
  ).
  settings(scalaxbSettings: _*).
  settings(
    slick <<= slickCodeGenTask,
    sourceGenerators in Compile <+= slick).
  settings(
    sourceGenerators in Compile += (scalaxb in Compile).taskValue,
    dispatchVersion in (Compile, scalaxb) := dispatchV,
    async in (Compile, scalaxb)           := true,
    xsdSource in (Compile, scalaxb) := file("./resources"),
    ScalaxbKeys.packageName in (Compile, scalaxb) := "xml"
    // packageNames in (Compile, scalaxb)    := Map(uri("http://schemas.microsoft.com/2003/10/Serialization/") -> "microsoft.serialization"),
    // logLevel in (Compile, scalaxb) := Level.Debug
  ).
  enablePlugins(PlayScala)

