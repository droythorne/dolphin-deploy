/**
 * Created by droythorne on 11/05/15.
 */

import com.hp.hpl.jena.ontology.OntModel
import org.specs2.mutable._
import play.api.Logger
import play.api.test.{PlaySpecification, FakeApplication}
import rdf.{MetalexTerms, DCTerms}

class RDFModelTest extends PlaySpecification {
  "DCTerms object" should {
    "successfully import the model" in {
      running(FakeApplication()) {
        val model: OntModel = DCTerms.m
        model.listClasses() must not be empty
      }
    }
    "get the title annotation property" in {
      running(FakeApplication()) {
        val ns = DCTerms.NS
        Logger.info("DCTerms namespace: " + ns)
        val title : String = DCTerms.title.getURI
        Logger.info("DCTerms title URI" + title)
        title must beEqualTo("http://purl.org/dc/terms/title")
      }
    }
  }
  "MetalexTerms object" should {
    "successfully import the model" in {
      running(FakeApplication()) {
        val model: OntModel = MetalexTerms.m
        Logger.info(model.listClasses().toList.toString)
        model.listClasses().toList must not be empty
      }
    }
    "get the BibliographicWork annotation property" in {
      running(FakeApplication()) {
        val bibliographic_object_uri = MetalexTerms.BibliographicWork.getURI
        Logger.info("Metalex namespace: " + MetalexTerms.NS)
        Logger.info("BibliographicObject URI: " + bibliographic_object_uri)
        bibliographic_object_uri must beEqualTo("http://www.metalex.eu/metalex/2008-05-02#BibliographicWork")
      }
    }
  }
}
