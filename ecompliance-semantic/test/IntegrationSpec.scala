import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.libs.iteratee.Iteratee
import play.api.mvc
import play.api.mvc.{Action, AnyContentAsText}

import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.Future
import scala.xml.XML

/**
 * add your integration spec here.
 * An integration test will fire up a whole play application in a real (or headless) browser
 */
@RunWith(classOf[JUnitRunner])
class IntegrationSpec extends Specification {

  "Application" should {

    "work from within a browser (TRIVIALLY TRUE MOCK)" in new WithBrowser {

      browser.goTo("http://localhost:" + port)

      //browser.pageSource must contain("")
      "browser successfully returned page" >> ok
    }
    "respond to SKOS freetext with SKOSConceptOccurence xml" in {
      val controller = controllers.SKOS
      val request = FakeRequest(POST, "", new FakeHeaders(), "lalala" : String)
      val action: Action[String] = controller.freetext()
      val result: Future[mvc.Result] = action(request)
      val xml_result = scalaxb.fromXML[xml.SKOSConceptOccurrence](XML.loadString(contentAsString(result)))
      println(xml_result)
      status(result) must equalTo(OK)
    }
  }
}
