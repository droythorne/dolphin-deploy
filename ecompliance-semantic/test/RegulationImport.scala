import akka.util.Timeout
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._
import dao.RegulationDAO
import scala.concurrent.duration._
/**
 * Created by droythorne on 10/05/15.
 */
@RunWith(classOf[JUnitRunner])
class RegulationImport extends PlaySpecification {
  override implicit def defaultAwaitTimeout: Timeout = 20.seconds
  "RegulationDAO" should {
    "produce a sequence of regulation ids and their children from creation tool database" in {
      running(FakeApplication()) {
        val dao = new RegulationDAO()
        val subregs = await(dao.root_regulations())
        "subregulations retrieved from database" >> ok
      }
    }
    "fetch a sequence of regulations and convert them to rdf" in {
      running(FakeApplication()) {
        val dao = new RegulationDAO()
        val regulations = await(dao.all())
        println(regulations)
        "all regulations retrieved from database" >> ok
      }
    }
  }
}
