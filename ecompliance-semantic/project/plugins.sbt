resolvers += Resolver.typesafeIvyRepo("releases")

resolvers += Resolver.typesafeIvyRepo("snapshots")

resolvers += Resolver.typesafeRepo("releases")

resolvers += Resolver.typesafeRepo("snapshots")

resolvers += Resolver.sonatypeRepo("public")

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.0-RC1" exclude("org.slf4j", "slf4j-simple"))

addSbtPlugin("org.scalaxb" % "sbt-scalaxb" % "1.3.0")